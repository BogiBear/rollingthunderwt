//
//  Mission.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/12/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit

class Mission {
    
    // MARK: Properties
    var name: String?
    var missionType: String?
    var startDate: Date?
    var airframe: String?
    var completed: Bool?
    
    init?(name: String, description: String, startDate: Date, airframe: String) {
        guard !name.isEmpty else {
            fatalError("Please provide a name for the Mission Object")
        }
        
        self.name = name
        self.missionType = description
        self.startDate = startDate
        self.airframe = airframe
    }
}
