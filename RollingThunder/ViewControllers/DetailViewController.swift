//
//  ViewController.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/4/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit
import os.log

class DetailViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: Properties
    @IBOutlet weak var txtWeaponName: UITextField!
    @IBOutlet weak var weaponImage: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    // MARK: Variables
    var weapon: WeaponSystem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        txtWeaponName.delegate = self
        
        if let weapon = weapon {
            navigationItem.title = weapon.name
            weaponImage.image = weapon.weaponPhoto
            ratingControl.rating = weapon.userRating!
        }
        
        updateSaveButtonState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtWeaponName.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = txtWeaponName.text
    }
    
    // Mark: UIImagePickerControllerDelegate method override
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary of Images but was given: \(info)")
        }
        
        weaponImage.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Private Methods
    private func updateSaveButtonState() {
        let text = txtWeaponName.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        let name = txtWeaponName.text ?? ""
        let photo = weaponImage.image
        let rating = ratingControl.rating
        
        weapon = WeaponSystem(name: name, weaponPhoto: photo!, userRating: rating)
    }
    
    // MARK: Actions
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        // verify which way was this view presented and dismiss accordingly if in add or Edit Mode
        let isPresentingInAddMode = presentingViewController is UINavigationController
        if isPresentingInAddMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationControler = navigationController {
            owningNavigationControler.popViewController(animated: true)
        }
        else {
            fatalError("Detail Navigation controller is not in a view Controller ")
        }
        
    }
    
    @IBAction func selectImageFromLibrary(_ sender: UITapGestureRecognizer) {
        txtWeaponName.resignFirstResponder()
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
}

