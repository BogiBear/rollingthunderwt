//
//  WeaponTypeCell.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/6/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit
import os.log

class WeaponTypeCell: UITableViewCell {
    
    // Mark: Properties
    @IBOutlet weak var weaponNameLabel: UILabel!
    @IBOutlet weak var weaponPhoto: UIImageView!
    @IBOutlet weak var weaponRating: RatingControl!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
