//
//  MissionTableViewCell.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/17/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit

class MissionTableViewCell: UITableViewCell {

    // MARK: UI Outlets
    @IBOutlet weak var missionNameLabel: UILabel!
    @IBOutlet weak var missionTypeLabel: UILabel!
    @IBOutlet weak var completionStateImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
