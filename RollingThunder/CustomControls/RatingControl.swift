//
//  RatingControl.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/6/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {

    // Mark: Properties
    @IBInspectable var starSize: CGSize = CGSize(width: 44.0, height: 44.0) {
        didSet {
            setupRatingButtons()
        }
    }
    @IBInspectable var starCount: Int = 5 {
        didSet {
            setupRatingButtons()
        }
    }
    
    
    // Mark: Other variables
    private var ratingButtons = [UIButton] ()
    var rating = 0 {
        didSet {
            updateButtonSelectedStates()
        }
    }
    
    // Mark: Control initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRatingButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupRatingButtons()
    }
    
    // Mark: Private methods
    private func setupRatingButtons() {
        
        for button in ratingButtons {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        let bundle = Bundle(for: type(of: self))
        let filledStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named: "emptyStar", in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage(named: "highlightedStar", in: bundle, compatibleWith: self.traitCollection)
        
        for index in 0..<starCount {
            let button = UIButton()
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.highlighted, .selected])
            
            // setup constraints
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: starSize.width).isActive = true
            button.widthAnchor.constraint(equalToConstant: starSize.height).isActive = true
            
            // set the accessibility label
            button.accessibilityLabel = "Set \(index + 1) start rating"
            
            addArrangedSubview(button)
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchUpInside)
            ratingButtons.append(button)
        }
        updateButtonSelectedStates()
    }
    
    private func updateButtonSelectedStates() {
        for (index, button) in ratingButtons.enumerated() {
            button.isSelected = index < rating
            
            // Set the hint string for the currently selected star
            let hintString: String?
            if rating == index + 1 {
                hintString = "Tap to reset the rating to zero."
            } else {
                hintString = nil
            }
            
            // Calculate the value string
            let valueString: String
            switch (rating) {
            case 0:
                valueString = "No rating set."
            case 1:
                valueString = "1 star set."
            default:
                valueString = "\(rating) stars set."
            }
            
            // Assign the hint string and value string
            button.accessibilityHint = hintString
            button.accessibilityValue = valueString
        }
    }
    
    // Mark: Control actions
    @objc func ratingButtonTapped(button: UIButton) {
        guard let index = ratingButtons.index(of: button) else {
            fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        let selectedRating = index + 1
        if selectedRating == rating {
            rating = 0
        }
        else {
            rating = selectedRating
        }
    }
    
    
}
