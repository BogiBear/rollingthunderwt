//
//  WeaponTableController.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/10/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit
import os.log

class WeaponTableController: UITableViewController {
    // Mark: Properties
    var weapons = [WeaponSystem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = editButtonItem
        loadWeapons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return weapons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "weaponTypeIdentifier"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? WeaponTypeCell else {
            fatalError("the cell you chose is not a WeaponTypeCell")
        }
        let weapon = weapons[indexPath.row]
        cell.weaponNameLabel.text = weapon.name
        cell.weaponPhoto.image = weapon.weaponPhoto
        cell.weaponRating.rating = weapon.userRating!
        

        // Configure the cell...

        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            weapons.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }*/


    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? "") {
        case "ShowWeaponDetail":
            guard let detailViewController = segue.destination as? DetailViewController else {
                    fatalError("Unexpected Destination: \(segue.destination)")
            }
            
            guard let selectedCell = sender as? WeaponTypeCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("Selected Cell not displayed in the table")
            }
            
            let weapon = weapons[indexPath.row]
            detailViewController.weapon = weapon
        case "AddWeapon": break
            // we don't need to do anything here
        default:
            fatalError("Unexpected Segue followed: \(String(describing: segue.identifier))")
        }
    }
    
    // MARK: Private methods
    private func loadWeapons() {
        let photo = UIImage(named: "defaultPhoto")
        
        guard let weapon1 = WeaponSystem(name: "BK-90", weaponPhoto: photo!, userRating: 5)
            else {
                fatalError("Unable to instantiate weapon systems object")
            }
        
        weapons += [weapon1]
        
    }
    
    // MARK: Actions
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? DetailViewController, let weapon = sourceViewController.weapon {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                weapons[selectedIndexPath.row] = weapon
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {
                let newIndexPath = IndexPath(row: weapons.count, section: 0)
                weapons.append(weapon)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }

}
