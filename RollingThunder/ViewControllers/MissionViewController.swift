//
//  MissionViewController.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/17/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit

class MissionViewController: UIViewController, UITextFieldDelegate,UINavigationControllerDelegate {
    
    // MARK: Attributes
    
    @IBOutlet weak var missionNameText: UITextField!
    @IBOutlet weak var missionDescriptionText: UITextField!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var inProgressSwitch: UISwitch!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    // MARK: Variables
    var mission: Mission?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set delegates
        missionNameText.delegate = self
        missionDescriptionText.delegate = self
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = NSLocalizedString("Mission details", comment: "")
        stateLabel.text = NSLocalizedString("In Progress", comment: "")
        updateSaveButtonState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case missionNameText:
            missionDescriptionText.resignFirstResponder()
        case missionDescriptionText:
            missionNameText.resignFirstResponder()
        default:
            missionNameText.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = missionNameText.text
    }
    
    // MARK: Private methods
    // MARK: Private Methods
    private func updateSaveButtonState() {
        let missionName = missionNameText.text ?? ""
        saveButton.isEnabled = !missionName.isEmpty
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func setInProgressState(_ sender: UISwitch) {
        if inProgressSwitch.isOn {
            stateLabel.text = NSLocalizedString("Completed", comment: "")
        }
        else {
            stateLabel.text = NSLocalizedString("In Progress", comment: "")
        }
    }
    
    @IBAction func saveMission(_ sender: UIBarButtonItem) {
    }
    
}


