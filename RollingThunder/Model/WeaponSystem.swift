//
//  WeaponSystem.swift
//  RollingThunder
//
//  Created by Bogdan Tudosie on 12/6/17.
//  Copyright © 2017 Bogdan Tudosie. All rights reserved.
//

import UIKit

class WeaponSystem {
    var name: String?
    var weaponPhoto: UIImage?
    var userRating: Int?
    
    // Mark: Initialization
    init?(name: String, weaponPhoto: UIImage, userRating: Int) {
        
        // Note: guard can be used to enforce certain values before the code is executed
        guard !name.isEmpty else {
            return nil
        }
        
        guard (userRating >= 0) && (userRating <= 5) else {
            return nil
        }
        
        self.name = name
        self.weaponPhoto = weaponPhoto
        self.userRating = userRating
    }
}
